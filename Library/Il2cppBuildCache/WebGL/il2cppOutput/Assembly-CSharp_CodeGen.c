﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Bullet::Start()
extern void Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4 (void);
// 0x00000002 System.Void Bullet::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Bullet_OnTriggerEnter2D_m9D34F5DCB36704FBF573BD78A9181790DD6C3B86 (void);
// 0x00000003 System.Collections.IEnumerator Bullet::WaitBulletDestroy()
extern void Bullet_WaitBulletDestroy_m0775E3B2D448AE8B6D319E82C6C7344FB8646BDB (void);
// 0x00000004 System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x00000005 System.Void Bullet/<WaitBulletDestroy>d__5::.ctor(System.Int32)
extern void U3CWaitBulletDestroyU3Ed__5__ctor_m286BA5208A94E3B54CF808D5ED50F95A7395A434 (void);
// 0x00000006 System.Void Bullet/<WaitBulletDestroy>d__5::System.IDisposable.Dispose()
extern void U3CWaitBulletDestroyU3Ed__5_System_IDisposable_Dispose_mB8D5CAA9F18A1CFBDDC88D46EE09E20F54819BA6 (void);
// 0x00000007 System.Boolean Bullet/<WaitBulletDestroy>d__5::MoveNext()
extern void U3CWaitBulletDestroyU3Ed__5_MoveNext_m7CBDADA8389720CAC171D6EC404D815689A5107F (void);
// 0x00000008 System.Object Bullet/<WaitBulletDestroy>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitBulletDestroyU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16261E8717F72F84324127298B394CA7FC29642A (void);
// 0x00000009 System.Void Bullet/<WaitBulletDestroy>d__5::System.Collections.IEnumerator.Reset()
extern void U3CWaitBulletDestroyU3Ed__5_System_Collections_IEnumerator_Reset_mB71FC6F39FE3197B183CB5B2138F320513F1C4B4 (void);
// 0x0000000A System.Object Bullet/<WaitBulletDestroy>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CWaitBulletDestroyU3Ed__5_System_Collections_IEnumerator_get_Current_mA6AEEB3A2D706D2A817B963E84570C2144CA9EA4 (void);
// 0x0000000B System.Void CameraClamp::Start()
extern void CameraClamp_Start_m32F8A4024089C22708D9A5BED9A6B6A61308E1A9 (void);
// 0x0000000C System.Void CameraClamp::Update()
extern void CameraClamp_Update_m377AFA1DE492C44BA9B7CCB5BF0CBE738ACE9A40 (void);
// 0x0000000D System.Void CameraClamp::.ctor()
extern void CameraClamp__ctor_mF7FD3881433061D4616388BA3415457119B6627B (void);
// 0x0000000E System.Void ChaserMovement::Awake()
extern void ChaserMovement_Awake_mB0FEAE72E0E244B6C7CFF54A49C78F74BFC070CF (void);
// 0x0000000F System.Void ChaserMovement::FixedUpdate()
extern void ChaserMovement_FixedUpdate_mAF6E6FC8B0842A8093BB65D29280F5D8B3E0E62A (void);
// 0x00000010 System.Void ChaserMovement::UpdateTargetDirection()
extern void ChaserMovement_UpdateTargetDirection_m31A853B1812DBB354DB48A985ADA4171798C5C96 (void);
// 0x00000011 System.Void ChaserMovement::RotateTowardsTarget()
extern void ChaserMovement_RotateTowardsTarget_m00F97DBE9ACC579EF17BDA2F426F093598433EF6 (void);
// 0x00000012 System.Void ChaserMovement::SetVelocity()
extern void ChaserMovement_SetVelocity_m1E126D9EC07C9C51AA7F4A4C8745D14EBE1AFE4A (void);
// 0x00000013 System.Void ChaserMovement::.ctor()
extern void ChaserMovement__ctor_mD61F540F187E7C0F98D4A754C9FB61DE62A95A0D (void);
// 0x00000014 System.Void EndGameManager::ApplyInfoPlayAgain()
extern void EndGameManager_ApplyInfoPlayAgain_mBA71385D1C42365A40EC9553861555171E75BF4A (void);
// 0x00000015 System.Void EndGameManager::.ctor()
extern void EndGameManager__ctor_mFB1205AEEE98DE48A9CF728427296947FB92FA86 (void);
// 0x00000016 System.Void EnemyAttack::Start()
extern void EnemyAttack_Start_mF6EC413D7F5A8F9CA9C6197BEF713C804AD43BB5 (void);
// 0x00000017 System.Void EnemyAttack::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void EnemyAttack_OnCollisionEnter2D_mDD7A8DB2A1C7A005978B4AD3054C5D145FA448E1 (void);
// 0x00000018 System.Void EnemyAttack::.ctor()
extern void EnemyAttack__ctor_mC9F235199C3D3A8DE4EB00B0AD46F796B241FE08 (void);
// 0x00000019 System.Void EnemyHealth::Start()
extern void EnemyHealth_Start_mD01F3D64A408764E38262C86167BA12B65D4A5D7 (void);
// 0x0000001A System.Void EnemyHealth::Update()
extern void EnemyHealth_Update_mBCBBEB935216A5E61B4F9A5F6833A09301C0E8DF (void);
// 0x0000001B System.Void EnemyHealth::TakeDamage(System.Int32)
extern void EnemyHealth_TakeDamage_m2F6DE42CE55261674A25F0DC5AF1A6C9931F8BC0 (void);
// 0x0000001C System.Void EnemyHealth::ExplosionEffect()
extern void EnemyHealth_ExplosionEffect_mA7C5E7CB9EC485171E6E8CCFABA22ECE774097B2 (void);
// 0x0000001D System.Collections.IEnumerator EnemyHealth::WaitStopEffect()
extern void EnemyHealth_WaitStopEffect_m7AFB312BF1F39286E30FBBB8788B19268F6FA150 (void);
// 0x0000001E System.Void EnemyHealth::.ctor()
extern void EnemyHealth__ctor_mF9FFC7A91A2AB12182655557BC05309E64E17AFE (void);
// 0x0000001F System.Void EnemyHealth/<WaitStopEffect>d__13::.ctor(System.Int32)
extern void U3CWaitStopEffectU3Ed__13__ctor_m25BAB6225F1E7EC970B81EA7CE7207556BAAAA6F (void);
// 0x00000020 System.Void EnemyHealth/<WaitStopEffect>d__13::System.IDisposable.Dispose()
extern void U3CWaitStopEffectU3Ed__13_System_IDisposable_Dispose_mA569F69F32CAAFCE951DEC553845A4A3F361FE30 (void);
// 0x00000021 System.Boolean EnemyHealth/<WaitStopEffect>d__13::MoveNext()
extern void U3CWaitStopEffectU3Ed__13_MoveNext_m6695825AC267204876620FC6D6DF45E44A0F6346 (void);
// 0x00000022 System.Object EnemyHealth/<WaitStopEffect>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitStopEffectU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C77661935107924964D38F2A329514FE4255C49 (void);
// 0x00000023 System.Void EnemyHealth/<WaitStopEffect>d__13::System.Collections.IEnumerator.Reset()
extern void U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_Reset_mBC6D7E680AA95FA5A01BA2017506EF9F8F765B91 (void);
// 0x00000024 System.Object EnemyHealth/<WaitStopEffect>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_get_Current_m050B658A87C16447D307758F9F9612E9D0DD7526 (void);
// 0x00000025 System.Void GameStartText::Start()
extern void GameStartText_Start_m4B8D96915A0701922948402355774FD4CD407A05 (void);
// 0x00000026 System.Collections.IEnumerator GameStartText::WaitDestroyText()
extern void GameStartText_WaitDestroyText_m0F96D053E959B5A024CECC30351C3F6A997E8E38 (void);
// 0x00000027 System.Void GameStartText::.ctor()
extern void GameStartText__ctor_m6C90648EFB2C37A584F2113D6D7EB437505DC094 (void);
// 0x00000028 System.Void GameStartText/<WaitDestroyText>d__2::.ctor(System.Int32)
extern void U3CWaitDestroyTextU3Ed__2__ctor_m4EE3707DEBBD9318CD638EC47177E127FE6FBEF8 (void);
// 0x00000029 System.Void GameStartText/<WaitDestroyText>d__2::System.IDisposable.Dispose()
extern void U3CWaitDestroyTextU3Ed__2_System_IDisposable_Dispose_m925D7E09D270F2A418F981B44857E9A889B57CC7 (void);
// 0x0000002A System.Boolean GameStartText/<WaitDestroyText>d__2::MoveNext()
extern void U3CWaitDestroyTextU3Ed__2_MoveNext_mE9B6F1AF14F668EE2D7595FC6C61F3E514D9F7B6 (void);
// 0x0000002B System.Object GameStartText/<WaitDestroyText>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitDestroyTextU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A989809CC89573598A73A570CB9603F7C36FD80 (void);
// 0x0000002C System.Void GameStartText/<WaitDestroyText>d__2::System.Collections.IEnumerator.Reset()
extern void U3CWaitDestroyTextU3Ed__2_System_Collections_IEnumerator_Reset_m64C3628E3810E960E132B92284D2708E60E236EC (void);
// 0x0000002D System.Object GameStartText/<WaitDestroyText>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CWaitDestroyTextU3Ed__2_System_Collections_IEnumerator_get_Current_m4E7F0D5E9D8177DC82EEBDF88FD4C6B38BD0BA3B (void);
// 0x0000002E System.Void GameTimer::Start()
extern void GameTimer_Start_mF3FE7822304FF256B5C515E9C2BB546D4719CD6B (void);
// 0x0000002F System.Void GameTimer::Update()
extern void GameTimer_Update_mB72832A94E2A7DD167FE2D477A32105A5AD7AF3E (void);
// 0x00000030 System.Void GameTimer::CountTimer()
extern void GameTimer_CountTimer_m991ECE931A82AF7C3A5E000584439632AA408662 (void);
// 0x00000031 System.Void GameTimer::AddScore()
extern void GameTimer_AddScore_mE2639B8B64849EA11516C2FB01F5864DFF04F32F (void);
// 0x00000032 System.Void GameTimer::EndGame()
extern void GameTimer_EndGame_mF0DB7B893797BCDBD99CADDB45A1CA41A6E7C8F7 (void);
// 0x00000033 System.Void GameTimer::.ctor()
extern void GameTimer__ctor_m23A2468C67D117AAB6E186E39E90F5A3AA160354 (void);
// 0x00000034 System.Void MenuManager::ConfigButton()
extern void MenuManager_ConfigButton_m2A6EC7B787F79591DA240DD132F1AD2FDE0191AE (void);
// 0x00000035 System.Void MenuManager::BackButton()
extern void MenuManager_BackButton_m989C81FC79C411207547BA86EAF72D4464C86D2C (void);
// 0x00000036 System.Void MenuManager::.ctor()
extern void MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529 (void);
// 0x00000037 System.Void NavigationMenu::SwitchScene(System.String)
extern void NavigationMenu_SwitchScene_mB01ED1DEBB5AB10FA5FB690A73FE08592A6A1CF9 (void);
// 0x00000038 System.Void NavigationMenu::.ctor()
extern void NavigationMenu__ctor_m9B8F6E28207A477A2D14C2B2EA874E18AB62A0E1 (void);
// 0x00000039 System.Boolean PlayerAwarenessController::get_AwareOfPlayer()
extern void PlayerAwarenessController_get_AwareOfPlayer_m808D36CFC9C67A89EBA9A719F8776B4B42249A37 (void);
// 0x0000003A System.Void PlayerAwarenessController::set_AwareOfPlayer(System.Boolean)
extern void PlayerAwarenessController_set_AwareOfPlayer_mD960C1962FE349F24BB1C0B5B8AB9D462705282D (void);
// 0x0000003B UnityEngine.Vector2 PlayerAwarenessController::get_DirectionToPlayer()
extern void PlayerAwarenessController_get_DirectionToPlayer_m374F8BB97E16ECE6CDA273AE8CDDEF21BC5B1D4B (void);
// 0x0000003C System.Void PlayerAwarenessController::set_DirectionToPlayer(UnityEngine.Vector2)
extern void PlayerAwarenessController_set_DirectionToPlayer_m9A38FAF42D36599FA666ABCC6248F55AA3751D55 (void);
// 0x0000003D System.Void PlayerAwarenessController::Awake()
extern void PlayerAwarenessController_Awake_m17BBC0869B888586B5E886D1AF37ACAC510CD181 (void);
// 0x0000003E System.Void PlayerAwarenessController::Update()
extern void PlayerAwarenessController_Update_m517B00B208A6B8A085A84AFF4E37FC59EF15F5EA (void);
// 0x0000003F System.Void PlayerAwarenessController::.ctor()
extern void PlayerAwarenessController__ctor_mB0ADEE8884AB1AF178D7BF7FC7645150896F35EF (void);
// 0x00000040 System.Void PlayerHealth::Start()
extern void PlayerHealth_Start_m78FD812EF2B87E9EC7A405A1BBB6ECB27BFF3589 (void);
// 0x00000041 System.Void PlayerHealth::Update()
extern void PlayerHealth_Update_m4ACD2FDDEBE8DC21C71BB853A975D03DD65061B3 (void);
// 0x00000042 System.Void PlayerHealth::TakeDamage(System.Int32)
extern void PlayerHealth_TakeDamage_m21F4EA70549D145406E078F0543A934DE498FDB7 (void);
// 0x00000043 System.Collections.IEnumerator PlayerHealth::DelayPlayerDead()
extern void PlayerHealth_DelayPlayerDead_mA6D9C7AC8E6D523693562F02978B4ADE8F0A995B (void);
// 0x00000044 System.Void PlayerHealth::.ctor()
extern void PlayerHealth__ctor_mE9AF3CA69205909E44287664BEAE503EC43875F1 (void);
// 0x00000045 System.Void PlayerHealth/<DelayPlayerDead>d__12::.ctor(System.Int32)
extern void U3CDelayPlayerDeadU3Ed__12__ctor_mF6B169A6528ADBCCEBC674C0B28773255B4DD597 (void);
// 0x00000046 System.Void PlayerHealth/<DelayPlayerDead>d__12::System.IDisposable.Dispose()
extern void U3CDelayPlayerDeadU3Ed__12_System_IDisposable_Dispose_m3B1E236249AE3937E11BF987AEDF6FAAA47FAF3F (void);
// 0x00000047 System.Boolean PlayerHealth/<DelayPlayerDead>d__12::MoveNext()
extern void U3CDelayPlayerDeadU3Ed__12_MoveNext_mD97FF0F224987C161F51FC96567A25293EE5042C (void);
// 0x00000048 System.Object PlayerHealth/<DelayPlayerDead>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayPlayerDeadU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m948B51A90B60A4915C3BCF85B7AF9703559F0115 (void);
// 0x00000049 System.Void PlayerHealth/<DelayPlayerDead>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDelayPlayerDeadU3Ed__12_System_Collections_IEnumerator_Reset_m232752AF09DE77FBA534ECD9C477EA705F0658D6 (void);
// 0x0000004A System.Object PlayerHealth/<DelayPlayerDead>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDelayPlayerDeadU3Ed__12_System_Collections_IEnumerator_get_Current_mF7E5B2772903E4EF64D175949F66F86842CF6340 (void);
// 0x0000004B System.Void PlayerHealthBar::SetMaxHealth(System.Int32)
extern void PlayerHealthBar_SetMaxHealth_m03114DEE8ECF7CE1B89BEB538727214405CFD928 (void);
// 0x0000004C System.Void PlayerHealthBar::SetHealth(System.Int32)
extern void PlayerHealthBar_SetHealth_m0DAFB2D772E868BCC1ECC19899D30E21E4D8EA68 (void);
// 0x0000004D System.Void PlayerHealthBar::.ctor()
extern void PlayerHealthBar__ctor_mEACC4D691595B746969FF17784DC787E96B9B59B (void);
// 0x0000004E System.Void PlayerMovement::Awake()
extern void PlayerMovement_Awake_m441E25AABE54B8C5068808DB8025B67B9A7EA87E (void);
// 0x0000004F System.Void PlayerMovement::FixedUpdate()
extern void PlayerMovement_FixedUpdate_m774280268A537B6ED9D9171CEAE67E9A0C3A9499 (void);
// 0x00000050 System.Void PlayerMovement::SetPlayerVelocity()
extern void PlayerMovement_SetPlayerVelocity_m1FAE4469D92DF59B7D3D8CE5441E6DE1FDC17131 (void);
// 0x00000051 System.Void PlayerMovement::RotateInDirectionOfInput()
extern void PlayerMovement_RotateInDirectionOfInput_m042F70AF701A83F30791AD2B42DD0CD8BD1292B0 (void);
// 0x00000052 System.Void PlayerMovement::OnMove(UnityEngine.InputSystem.InputValue)
extern void PlayerMovement_OnMove_m881AB6D31F026140EC8CA65E045B259D74352F76 (void);
// 0x00000053 System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
// 0x00000054 System.Void PlayerShoot::Start()
extern void PlayerShoot_Start_m8C6B42F5F94183035D1D6EC5BDE42889D55E1C0B (void);
// 0x00000055 System.Void PlayerShoot::Update()
extern void PlayerShoot_Update_m4B00823B72F562E9FD986F95E097D90096AD01F7 (void);
// 0x00000056 System.Void PlayerShoot::FrontalBullet()
extern void PlayerShoot_FrontalBullet_mC8C7EAB9080E121FEEB76783E39C2383B3967059 (void);
// 0x00000057 System.Void PlayerShoot::SideBullet()
extern void PlayerShoot_SideBullet_m57A603AD32CDE0CC93BA1F0BBBA63DBDA11C16AD (void);
// 0x00000058 System.Void PlayerShoot::OnFire(UnityEngine.InputSystem.InputValue)
extern void PlayerShoot_OnFire_m3647DF10901C0B903893373D285BA760FE92EAC9 (void);
// 0x00000059 System.Collections.IEnumerator PlayerShoot::StopParticleEffectSideShoot()
extern void PlayerShoot_StopParticleEffectSideShoot_m59AE2120CEC056AFB7A3D12CA344C74450D71659 (void);
// 0x0000005A System.Collections.IEnumerator PlayerShoot::StopParticleEffectFrontShoot()
extern void PlayerShoot_StopParticleEffectFrontShoot_m79BF3B20D0DD0334E19ED056698B3E3C2ED47FD8 (void);
// 0x0000005B System.Void PlayerShoot::.ctor()
extern void PlayerShoot__ctor_m45F3303F838D71340ECB70A6E66FF47ADE4E850C (void);
// 0x0000005C System.Void PlayerShoot/<StopParticleEffectSideShoot>d__17::.ctor(System.Int32)
extern void U3CStopParticleEffectSideShootU3Ed__17__ctor_m0EDF46F07A86C43D7BB7A20CC785D9ED4E815C6D (void);
// 0x0000005D System.Void PlayerShoot/<StopParticleEffectSideShoot>d__17::System.IDisposable.Dispose()
extern void U3CStopParticleEffectSideShootU3Ed__17_System_IDisposable_Dispose_mB4018D7722BE2F086D92131BF78379CA705A8D8E (void);
// 0x0000005E System.Boolean PlayerShoot/<StopParticleEffectSideShoot>d__17::MoveNext()
extern void U3CStopParticleEffectSideShootU3Ed__17_MoveNext_m0551C4E27F3A329C2C672FC114546B28EC80ACCE (void);
// 0x0000005F System.Object PlayerShoot/<StopParticleEffectSideShoot>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopParticleEffectSideShootU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2416657988A502BC6D7D3857DE7B050BA23733F0 (void);
// 0x00000060 System.Void PlayerShoot/<StopParticleEffectSideShoot>d__17::System.Collections.IEnumerator.Reset()
extern void U3CStopParticleEffectSideShootU3Ed__17_System_Collections_IEnumerator_Reset_mE44CD0E292E1FEE02F9326387ACF7348BF007555 (void);
// 0x00000061 System.Object PlayerShoot/<StopParticleEffectSideShoot>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CStopParticleEffectSideShootU3Ed__17_System_Collections_IEnumerator_get_Current_m70B2EE31947590E53B89FB8649591272729886DD (void);
// 0x00000062 System.Void PlayerShoot/<StopParticleEffectFrontShoot>d__18::.ctor(System.Int32)
extern void U3CStopParticleEffectFrontShootU3Ed__18__ctor_mDD32AAF3222FC81262D87B57F6D4D631E50044C9 (void);
// 0x00000063 System.Void PlayerShoot/<StopParticleEffectFrontShoot>d__18::System.IDisposable.Dispose()
extern void U3CStopParticleEffectFrontShootU3Ed__18_System_IDisposable_Dispose_m167CDDB19475CA486AA0FB82CD6CF1B4ABEAD1F6 (void);
// 0x00000064 System.Boolean PlayerShoot/<StopParticleEffectFrontShoot>d__18::MoveNext()
extern void U3CStopParticleEffectFrontShootU3Ed__18_MoveNext_mEB5E5760310FC809B0FBB0C1266218019DB6850A (void);
// 0x00000065 System.Object PlayerShoot/<StopParticleEffectFrontShoot>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AA28E30C6546F2B00E5E19286F4CFA1131C94E4 (void);
// 0x00000066 System.Void PlayerShoot/<StopParticleEffectFrontShoot>d__18::System.Collections.IEnumerator.Reset()
extern void U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_IEnumerator_Reset_m896298A7E7BE8E12821FB96FA2149710F3AC48B2 (void);
// 0x00000067 System.Object PlayerShoot/<StopParticleEffectFrontShoot>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_IEnumerator_get_Current_mF3837D5FF2583A087BEE2EC278E68FE000787929 (void);
// 0x00000068 System.Void ShooterEnemy::Start()
extern void ShooterEnemy_Start_mF08A92E9F4E04D69D68050564A31EA2FFA25B2D6 (void);
// 0x00000069 System.Void ShooterEnemy::Update()
extern void ShooterEnemy_Update_mBB81A338049119DBFADD6824FFF4D5A672F249DD (void);
// 0x0000006A System.Void ShooterEnemy::UpdateTargetDirection()
extern void ShooterEnemy_UpdateTargetDirection_m6DE6E5699527ACEB03C0EF80A38D72ADC95064A8 (void);
// 0x0000006B System.Void ShooterEnemy::LookAtPlayer()
extern void ShooterEnemy_LookAtPlayer_mF6DCB6262783E9BC80D4342C0198C4F692BC37E2 (void);
// 0x0000006C System.Void ShooterEnemy::ClampCamera()
extern void ShooterEnemy_ClampCamera_mD702FD51F4282F858CA992AC7C5077BC5A7A5BC4 (void);
// 0x0000006D System.Void ShooterEnemy::.ctor()
extern void ShooterEnemy__ctor_mEFEA3396316C6A280401270D83FD70FC24845BF2 (void);
// 0x0000006E System.Void ShooterHealth::Start()
extern void ShooterHealth_Start_m2CFAEA574CBACC5FB6F311CF63F227953F24CBE3 (void);
// 0x0000006F System.Void ShooterHealth::Update()
extern void ShooterHealth_Update_mA1846DE06F9E0C43C9DB0F70A306AF6A9EA84E1C (void);
// 0x00000070 System.Void ShooterHealth::TakeDamage(System.Int32)
extern void ShooterHealth_TakeDamage_m52AAF6E7701BE852CDA33279A5A12D1E0987BFCE (void);
// 0x00000071 System.Void ShooterHealth::ExplosionEffect()
extern void ShooterHealth_ExplosionEffect_m78E44749B0F2A2263A59D8698975C6A98F5AD834 (void);
// 0x00000072 System.Collections.IEnumerator ShooterHealth::WaitStopEffect()
extern void ShooterHealth_WaitStopEffect_m60C7351C6F107A74783EDC0F89A74E8F3387E8DB (void);
// 0x00000073 System.Void ShooterHealth::.ctor()
extern void ShooterHealth__ctor_m2329BBD48FD4A76103FD8730F1EEB5C423147CAF (void);
// 0x00000074 System.Void ShooterHealth/<WaitStopEffect>d__13::.ctor(System.Int32)
extern void U3CWaitStopEffectU3Ed__13__ctor_m4325A1AEF8E24D44DB5D59ED9FC54AA0097468CA (void);
// 0x00000075 System.Void ShooterHealth/<WaitStopEffect>d__13::System.IDisposable.Dispose()
extern void U3CWaitStopEffectU3Ed__13_System_IDisposable_Dispose_m619F2D15380D5AD53059C57749B7AD9E3EAD3767 (void);
// 0x00000076 System.Boolean ShooterHealth/<WaitStopEffect>d__13::MoveNext()
extern void U3CWaitStopEffectU3Ed__13_MoveNext_m4FBB2D1CF1453F6127B0488B3111D89469093A59 (void);
// 0x00000077 System.Object ShooterHealth/<WaitStopEffect>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitStopEffectU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m118EAC4268315A97C04F570D43B4E7FD6448DBCC (void);
// 0x00000078 System.Void ShooterHealth/<WaitStopEffect>d__13::System.Collections.IEnumerator.Reset()
extern void U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_Reset_mE457C9E0B5212F233CF4801C0C9320B0DC50DB42 (void);
// 0x00000079 System.Object ShooterHealth/<WaitStopEffect>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_get_Current_m45FC3FB3CC22D71B8E8C90B684B0B98AE2DD2C9D (void);
// 0x0000007A System.Void ShooterProjectile::Start()
extern void ShooterProjectile_Start_mA130EB6FAE9EAC4C8074777E968793FD4E5357A4 (void);
// 0x0000007B System.Void ShooterProjectile::Update()
extern void ShooterProjectile_Update_m85B4040F4A6B022A3B1895625EB08921DEFAE84C (void);
// 0x0000007C System.Void ShooterProjectile::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ShooterProjectile_OnTriggerEnter2D_mBB5F2B89D73CCF8E5B608F6AC9D8E299D78DFC03 (void);
// 0x0000007D System.Void ShooterProjectile::DestroyProjectile()
extern void ShooterProjectile_DestroyProjectile_m66ACB6087F1F581F796BDA7D5D2887416CC8F33C (void);
// 0x0000007E System.Void ShooterProjectile::.ctor()
extern void ShooterProjectile__ctor_m70197C6B45EBFE740AC8E3911630A2167CF0CB74 (void);
// 0x0000007F System.Void TimerConfigDropdown::Awake()
extern void TimerConfigDropdown_Awake_m7C4A9892AEB39F3336544923A4970A439476B100 (void);
// 0x00000080 System.Void TimerConfigDropdown::HandleInputData(System.Int32)
extern void TimerConfigDropdown_HandleInputData_m325BC463857D8F7C2EB987D595DDEA526E032485 (void);
// 0x00000081 System.Void TimerConfigDropdown::WaveInputData(System.Int32)
extern void TimerConfigDropdown_WaveInputData_m36764E0FAAF560BD1B875DDA27F33D042FFDD831 (void);
// 0x00000082 System.Void TimerConfigDropdown::.ctor()
extern void TimerConfigDropdown__ctor_m0D4500E16BB517B22808F830362D07724A4EA350 (void);
// 0x00000083 System.Void WaveSpawner::Update()
extern void WaveSpawner_Update_m4E15356E6D57C172B7186BB434E8FDDABF403805 (void);
// 0x00000084 System.Void WaveSpawner::SpawnWave()
extern void WaveSpawner_SpawnWave_m37CE43A01031254E92A2C60182804B88B92D25DE (void);
// 0x00000085 System.Void WaveSpawner::SpawnEnemy()
extern void WaveSpawner_SpawnEnemy_mCF0DB7021CBD10390E9E26AED253AF9555062DC4 (void);
// 0x00000086 System.Void WaveSpawner::.ctor()
extern void WaveSpawner__ctor_mBF1BD891AD3A00B8CFCC8C8044AFA59DCCD2C954 (void);
static Il2CppMethodPointer s_methodPointers[134] = 
{
	Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4,
	Bullet_OnTriggerEnter2D_m9D34F5DCB36704FBF573BD78A9181790DD6C3B86,
	Bullet_WaitBulletDestroy_m0775E3B2D448AE8B6D319E82C6C7344FB8646BDB,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	U3CWaitBulletDestroyU3Ed__5__ctor_m286BA5208A94E3B54CF808D5ED50F95A7395A434,
	U3CWaitBulletDestroyU3Ed__5_System_IDisposable_Dispose_mB8D5CAA9F18A1CFBDDC88D46EE09E20F54819BA6,
	U3CWaitBulletDestroyU3Ed__5_MoveNext_m7CBDADA8389720CAC171D6EC404D815689A5107F,
	U3CWaitBulletDestroyU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16261E8717F72F84324127298B394CA7FC29642A,
	U3CWaitBulletDestroyU3Ed__5_System_Collections_IEnumerator_Reset_mB71FC6F39FE3197B183CB5B2138F320513F1C4B4,
	U3CWaitBulletDestroyU3Ed__5_System_Collections_IEnumerator_get_Current_mA6AEEB3A2D706D2A817B963E84570C2144CA9EA4,
	CameraClamp_Start_m32F8A4024089C22708D9A5BED9A6B6A61308E1A9,
	CameraClamp_Update_m377AFA1DE492C44BA9B7CCB5BF0CBE738ACE9A40,
	CameraClamp__ctor_mF7FD3881433061D4616388BA3415457119B6627B,
	ChaserMovement_Awake_mB0FEAE72E0E244B6C7CFF54A49C78F74BFC070CF,
	ChaserMovement_FixedUpdate_mAF6E6FC8B0842A8093BB65D29280F5D8B3E0E62A,
	ChaserMovement_UpdateTargetDirection_m31A853B1812DBB354DB48A985ADA4171798C5C96,
	ChaserMovement_RotateTowardsTarget_m00F97DBE9ACC579EF17BDA2F426F093598433EF6,
	ChaserMovement_SetVelocity_m1E126D9EC07C9C51AA7F4A4C8745D14EBE1AFE4A,
	ChaserMovement__ctor_mD61F540F187E7C0F98D4A754C9FB61DE62A95A0D,
	EndGameManager_ApplyInfoPlayAgain_mBA71385D1C42365A40EC9553861555171E75BF4A,
	EndGameManager__ctor_mFB1205AEEE98DE48A9CF728427296947FB92FA86,
	EnemyAttack_Start_mF6EC413D7F5A8F9CA9C6197BEF713C804AD43BB5,
	EnemyAttack_OnCollisionEnter2D_mDD7A8DB2A1C7A005978B4AD3054C5D145FA448E1,
	EnemyAttack__ctor_mC9F235199C3D3A8DE4EB00B0AD46F796B241FE08,
	EnemyHealth_Start_mD01F3D64A408764E38262C86167BA12B65D4A5D7,
	EnemyHealth_Update_mBCBBEB935216A5E61B4F9A5F6833A09301C0E8DF,
	EnemyHealth_TakeDamage_m2F6DE42CE55261674A25F0DC5AF1A6C9931F8BC0,
	EnemyHealth_ExplosionEffect_mA7C5E7CB9EC485171E6E8CCFABA22ECE774097B2,
	EnemyHealth_WaitStopEffect_m7AFB312BF1F39286E30FBBB8788B19268F6FA150,
	EnemyHealth__ctor_mF9FFC7A91A2AB12182655557BC05309E64E17AFE,
	U3CWaitStopEffectU3Ed__13__ctor_m25BAB6225F1E7EC970B81EA7CE7207556BAAAA6F,
	U3CWaitStopEffectU3Ed__13_System_IDisposable_Dispose_mA569F69F32CAAFCE951DEC553845A4A3F361FE30,
	U3CWaitStopEffectU3Ed__13_MoveNext_m6695825AC267204876620FC6D6DF45E44A0F6346,
	U3CWaitStopEffectU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C77661935107924964D38F2A329514FE4255C49,
	U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_Reset_mBC6D7E680AA95FA5A01BA2017506EF9F8F765B91,
	U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_get_Current_m050B658A87C16447D307758F9F9612E9D0DD7526,
	GameStartText_Start_m4B8D96915A0701922948402355774FD4CD407A05,
	GameStartText_WaitDestroyText_m0F96D053E959B5A024CECC30351C3F6A997E8E38,
	GameStartText__ctor_m6C90648EFB2C37A584F2113D6D7EB437505DC094,
	U3CWaitDestroyTextU3Ed__2__ctor_m4EE3707DEBBD9318CD638EC47177E127FE6FBEF8,
	U3CWaitDestroyTextU3Ed__2_System_IDisposable_Dispose_m925D7E09D270F2A418F981B44857E9A889B57CC7,
	U3CWaitDestroyTextU3Ed__2_MoveNext_mE9B6F1AF14F668EE2D7595FC6C61F3E514D9F7B6,
	U3CWaitDestroyTextU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A989809CC89573598A73A570CB9603F7C36FD80,
	U3CWaitDestroyTextU3Ed__2_System_Collections_IEnumerator_Reset_m64C3628E3810E960E132B92284D2708E60E236EC,
	U3CWaitDestroyTextU3Ed__2_System_Collections_IEnumerator_get_Current_m4E7F0D5E9D8177DC82EEBDF88FD4C6B38BD0BA3B,
	GameTimer_Start_mF3FE7822304FF256B5C515E9C2BB546D4719CD6B,
	GameTimer_Update_mB72832A94E2A7DD167FE2D477A32105A5AD7AF3E,
	GameTimer_CountTimer_m991ECE931A82AF7C3A5E000584439632AA408662,
	GameTimer_AddScore_mE2639B8B64849EA11516C2FB01F5864DFF04F32F,
	GameTimer_EndGame_mF0DB7B893797BCDBD99CADDB45A1CA41A6E7C8F7,
	GameTimer__ctor_m23A2468C67D117AAB6E186E39E90F5A3AA160354,
	MenuManager_ConfigButton_m2A6EC7B787F79591DA240DD132F1AD2FDE0191AE,
	MenuManager_BackButton_m989C81FC79C411207547BA86EAF72D4464C86D2C,
	MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529,
	NavigationMenu_SwitchScene_mB01ED1DEBB5AB10FA5FB690A73FE08592A6A1CF9,
	NavigationMenu__ctor_m9B8F6E28207A477A2D14C2B2EA874E18AB62A0E1,
	PlayerAwarenessController_get_AwareOfPlayer_m808D36CFC9C67A89EBA9A719F8776B4B42249A37,
	PlayerAwarenessController_set_AwareOfPlayer_mD960C1962FE349F24BB1C0B5B8AB9D462705282D,
	PlayerAwarenessController_get_DirectionToPlayer_m374F8BB97E16ECE6CDA273AE8CDDEF21BC5B1D4B,
	PlayerAwarenessController_set_DirectionToPlayer_m9A38FAF42D36599FA666ABCC6248F55AA3751D55,
	PlayerAwarenessController_Awake_m17BBC0869B888586B5E886D1AF37ACAC510CD181,
	PlayerAwarenessController_Update_m517B00B208A6B8A085A84AFF4E37FC59EF15F5EA,
	PlayerAwarenessController__ctor_mB0ADEE8884AB1AF178D7BF7FC7645150896F35EF,
	PlayerHealth_Start_m78FD812EF2B87E9EC7A405A1BBB6ECB27BFF3589,
	PlayerHealth_Update_m4ACD2FDDEBE8DC21C71BB853A975D03DD65061B3,
	PlayerHealth_TakeDamage_m21F4EA70549D145406E078F0543A934DE498FDB7,
	PlayerHealth_DelayPlayerDead_mA6D9C7AC8E6D523693562F02978B4ADE8F0A995B,
	PlayerHealth__ctor_mE9AF3CA69205909E44287664BEAE503EC43875F1,
	U3CDelayPlayerDeadU3Ed__12__ctor_mF6B169A6528ADBCCEBC674C0B28773255B4DD597,
	U3CDelayPlayerDeadU3Ed__12_System_IDisposable_Dispose_m3B1E236249AE3937E11BF987AEDF6FAAA47FAF3F,
	U3CDelayPlayerDeadU3Ed__12_MoveNext_mD97FF0F224987C161F51FC96567A25293EE5042C,
	U3CDelayPlayerDeadU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m948B51A90B60A4915C3BCF85B7AF9703559F0115,
	U3CDelayPlayerDeadU3Ed__12_System_Collections_IEnumerator_Reset_m232752AF09DE77FBA534ECD9C477EA705F0658D6,
	U3CDelayPlayerDeadU3Ed__12_System_Collections_IEnumerator_get_Current_mF7E5B2772903E4EF64D175949F66F86842CF6340,
	PlayerHealthBar_SetMaxHealth_m03114DEE8ECF7CE1B89BEB538727214405CFD928,
	PlayerHealthBar_SetHealth_m0DAFB2D772E868BCC1ECC19899D30E21E4D8EA68,
	PlayerHealthBar__ctor_mEACC4D691595B746969FF17784DC787E96B9B59B,
	PlayerMovement_Awake_m441E25AABE54B8C5068808DB8025B67B9A7EA87E,
	PlayerMovement_FixedUpdate_m774280268A537B6ED9D9171CEAE67E9A0C3A9499,
	PlayerMovement_SetPlayerVelocity_m1FAE4469D92DF59B7D3D8CE5441E6DE1FDC17131,
	PlayerMovement_RotateInDirectionOfInput_m042F70AF701A83F30791AD2B42DD0CD8BD1292B0,
	PlayerMovement_OnMove_m881AB6D31F026140EC8CA65E045B259D74352F76,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
	PlayerShoot_Start_m8C6B42F5F94183035D1D6EC5BDE42889D55E1C0B,
	PlayerShoot_Update_m4B00823B72F562E9FD986F95E097D90096AD01F7,
	PlayerShoot_FrontalBullet_mC8C7EAB9080E121FEEB76783E39C2383B3967059,
	PlayerShoot_SideBullet_m57A603AD32CDE0CC93BA1F0BBBA63DBDA11C16AD,
	PlayerShoot_OnFire_m3647DF10901C0B903893373D285BA760FE92EAC9,
	PlayerShoot_StopParticleEffectSideShoot_m59AE2120CEC056AFB7A3D12CA344C74450D71659,
	PlayerShoot_StopParticleEffectFrontShoot_m79BF3B20D0DD0334E19ED056698B3E3C2ED47FD8,
	PlayerShoot__ctor_m45F3303F838D71340ECB70A6E66FF47ADE4E850C,
	U3CStopParticleEffectSideShootU3Ed__17__ctor_m0EDF46F07A86C43D7BB7A20CC785D9ED4E815C6D,
	U3CStopParticleEffectSideShootU3Ed__17_System_IDisposable_Dispose_mB4018D7722BE2F086D92131BF78379CA705A8D8E,
	U3CStopParticleEffectSideShootU3Ed__17_MoveNext_m0551C4E27F3A329C2C672FC114546B28EC80ACCE,
	U3CStopParticleEffectSideShootU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2416657988A502BC6D7D3857DE7B050BA23733F0,
	U3CStopParticleEffectSideShootU3Ed__17_System_Collections_IEnumerator_Reset_mE44CD0E292E1FEE02F9326387ACF7348BF007555,
	U3CStopParticleEffectSideShootU3Ed__17_System_Collections_IEnumerator_get_Current_m70B2EE31947590E53B89FB8649591272729886DD,
	U3CStopParticleEffectFrontShootU3Ed__18__ctor_mDD32AAF3222FC81262D87B57F6D4D631E50044C9,
	U3CStopParticleEffectFrontShootU3Ed__18_System_IDisposable_Dispose_m167CDDB19475CA486AA0FB82CD6CF1B4ABEAD1F6,
	U3CStopParticleEffectFrontShootU3Ed__18_MoveNext_mEB5E5760310FC809B0FBB0C1266218019DB6850A,
	U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AA28E30C6546F2B00E5E19286F4CFA1131C94E4,
	U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_IEnumerator_Reset_m896298A7E7BE8E12821FB96FA2149710F3AC48B2,
	U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_IEnumerator_get_Current_mF3837D5FF2583A087BEE2EC278E68FE000787929,
	ShooterEnemy_Start_mF08A92E9F4E04D69D68050564A31EA2FFA25B2D6,
	ShooterEnemy_Update_mBB81A338049119DBFADD6824FFF4D5A672F249DD,
	ShooterEnemy_UpdateTargetDirection_m6DE6E5699527ACEB03C0EF80A38D72ADC95064A8,
	ShooterEnemy_LookAtPlayer_mF6DCB6262783E9BC80D4342C0198C4F692BC37E2,
	ShooterEnemy_ClampCamera_mD702FD51F4282F858CA992AC7C5077BC5A7A5BC4,
	ShooterEnemy__ctor_mEFEA3396316C6A280401270D83FD70FC24845BF2,
	ShooterHealth_Start_m2CFAEA574CBACC5FB6F311CF63F227953F24CBE3,
	ShooterHealth_Update_mA1846DE06F9E0C43C9DB0F70A306AF6A9EA84E1C,
	ShooterHealth_TakeDamage_m52AAF6E7701BE852CDA33279A5A12D1E0987BFCE,
	ShooterHealth_ExplosionEffect_m78E44749B0F2A2263A59D8698975C6A98F5AD834,
	ShooterHealth_WaitStopEffect_m60C7351C6F107A74783EDC0F89A74E8F3387E8DB,
	ShooterHealth__ctor_m2329BBD48FD4A76103FD8730F1EEB5C423147CAF,
	U3CWaitStopEffectU3Ed__13__ctor_m4325A1AEF8E24D44DB5D59ED9FC54AA0097468CA,
	U3CWaitStopEffectU3Ed__13_System_IDisposable_Dispose_m619F2D15380D5AD53059C57749B7AD9E3EAD3767,
	U3CWaitStopEffectU3Ed__13_MoveNext_m4FBB2D1CF1453F6127B0488B3111D89469093A59,
	U3CWaitStopEffectU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m118EAC4268315A97C04F570D43B4E7FD6448DBCC,
	U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_Reset_mE457C9E0B5212F233CF4801C0C9320B0DC50DB42,
	U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_get_Current_m45FC3FB3CC22D71B8E8C90B684B0B98AE2DD2C9D,
	ShooterProjectile_Start_mA130EB6FAE9EAC4C8074777E968793FD4E5357A4,
	ShooterProjectile_Update_m85B4040F4A6B022A3B1895625EB08921DEFAE84C,
	ShooterProjectile_OnTriggerEnter2D_mBB5F2B89D73CCF8E5B608F6AC9D8E299D78DFC03,
	ShooterProjectile_DestroyProjectile_m66ACB6087F1F581F796BDA7D5D2887416CC8F33C,
	ShooterProjectile__ctor_m70197C6B45EBFE740AC8E3911630A2167CF0CB74,
	TimerConfigDropdown_Awake_m7C4A9892AEB39F3336544923A4970A439476B100,
	TimerConfigDropdown_HandleInputData_m325BC463857D8F7C2EB987D595DDEA526E032485,
	TimerConfigDropdown_WaveInputData_m36764E0FAAF560BD1B875DDA27F33D042FFDD831,
	TimerConfigDropdown__ctor_m0D4500E16BB517B22808F830362D07724A4EA350,
	WaveSpawner_Update_m4E15356E6D57C172B7186BB434E8FDDABF403805,
	WaveSpawner_SpawnWave_m37CE43A01031254E92A2C60182804B88B92D25DE,
	WaveSpawner_SpawnEnemy_mCF0DB7021CBD10390E9E26AED253AF9555062DC4,
	WaveSpawner__ctor_mBF1BD891AD3A00B8CFCC8C8044AFA59DCCD2C954,
};
static const int32_t s_InvokerIndices[134] = 
{
	2497,
	1967,
	2434,
	2497,
	1953,
	2497,
	2459,
	2434,
	2497,
	2434,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	1967,
	2497,
	2497,
	2497,
	1953,
	2497,
	2434,
	2497,
	1953,
	2497,
	2459,
	2434,
	2497,
	2434,
	2497,
	2434,
	2497,
	1953,
	2497,
	2459,
	2434,
	2497,
	2434,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	1967,
	2497,
	2459,
	1988,
	2490,
	2017,
	2497,
	2497,
	2497,
	2497,
	2497,
	1953,
	2434,
	2497,
	1953,
	2497,
	2459,
	2434,
	2497,
	2434,
	1953,
	1953,
	2497,
	2497,
	2497,
	2497,
	2497,
	1967,
	2497,
	2497,
	2497,
	2497,
	2497,
	1967,
	2434,
	2434,
	2497,
	1953,
	2497,
	2459,
	2434,
	2497,
	2434,
	1953,
	2497,
	2459,
	2434,
	2497,
	2434,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	2497,
	1953,
	2497,
	2434,
	2497,
	1953,
	2497,
	2459,
	2434,
	2497,
	2434,
	2497,
	2497,
	1967,
	2497,
	2497,
	2497,
	1953,
	1953,
	2497,
	2497,
	2497,
	2497,
	2497,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	134,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
