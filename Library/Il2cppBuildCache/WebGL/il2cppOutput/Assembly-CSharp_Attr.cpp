﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_Bullet_WaitBulletDestroy_m0775E3B2D448AE8B6D319E82C6C7344FB8646BDB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_0_0_0_var), NULL);
	}
}
static void U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5__ctor_m286BA5208A94E3B54CF808D5ED50F95A7395A434(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5_System_IDisposable_Dispose_mB8D5CAA9F18A1CFBDDC88D46EE09E20F54819BA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16261E8717F72F84324127298B394CA7FC29642A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5_System_Collections_IEnumerator_Reset_mB71FC6F39FE3197B183CB5B2138F320513F1C4B4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5_System_Collections_IEnumerator_get_Current_mA6AEEB3A2D706D2A817B963E84570C2144CA9EA4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CameraClamp_t39FDC82E849AB8EEB2C6D78717894966171C1047_CustomAttributesCacheGenerator__targetToFollow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChaserMovement_tB96C589A2E83EBFDA976369667C93F223DD929E0_CustomAttributesCacheGenerator__speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChaserMovement_tB96C589A2E83EBFDA976369667C93F223DD929E0_CustomAttributesCacheGenerator__rotationSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EndGameManager_t391E87B9BD3657182E84CF31EEC09F8019FF7B27_CustomAttributesCacheGenerator__getWave(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EndGameManager_t391E87B9BD3657182E84CF31EEC09F8019FF7B27_CustomAttributesCacheGenerator__getTimer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnemyAttack_t770C16819BCC010CF374A277F60DBE4B3F8467E2_CustomAttributesCacheGenerator__cooldownAttack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator__enemySprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator__deteriorationEnemy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator__anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator_EnemyHealth_WaitStopEffect_m7AFB312BF1F39286E30FBBB8788B19268F6FA150(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_0_0_0_var), NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13__ctor_m25BAB6225F1E7EC970B81EA7CE7207556BAAAA6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_IDisposable_Dispose_mA569F69F32CAAFCE951DEC553845A4A3F361FE30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C77661935107924964D38F2A329514FE4255C49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_Reset_mBC6D7E680AA95FA5A01BA2017506EF9F8F765B91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_get_Current_m050B658A87C16447D307758F9F9612E9D0DD7526(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GameStartText_t6B22BB56DBBF3991534D41C04BF6C17269FA180E_CustomAttributesCacheGenerator__startGameText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameStartText_t6B22BB56DBBF3991534D41C04BF6C17269FA180E_CustomAttributesCacheGenerator_GameStartText_WaitDestroyText_m0F96D053E959B5A024CECC30351C3F6A997E8E38(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_0_0_0_var), NULL);
	}
}
static void U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2__ctor_m4EE3707DEBBD9318CD638EC47177E127FE6FBEF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2_System_IDisposable_Dispose_m925D7E09D270F2A418F981B44857E9A889B57CC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A989809CC89573598A73A570CB9603F7C36FD80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2_System_Collections_IEnumerator_Reset_m64C3628E3810E960E132B92284D2708E60E236EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2_System_Collections_IEnumerator_get_Current_m4E7F0D5E9D8177DC82EEBDF88FD4C6B38BD0BA3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator_EndGameCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator__Player(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator__spawner(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator__scoreText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator__highScore(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_U3CAwareOfPlayerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_U3CDirectionToPlayerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator__playerAwarenessDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_PlayerAwarenessController_get_AwareOfPlayer_m808D36CFC9C67A89EBA9A719F8776B4B42249A37(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_PlayerAwarenessController_set_AwareOfPlayer_mD960C1962FE349F24BB1C0B5B8AB9D462705282D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_PlayerAwarenessController_get_DirectionToPlayer_m374F8BB97E16ECE6CDA273AE8CDDEF21BC5B1D4B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_PlayerAwarenessController_set_DirectionToPlayer_m9A38FAF42D36599FA666ABCC6248F55AA3751D55(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator__anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator__playerSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator__deteriorationPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator_PlayerHealth_DelayPlayerDead_mA6D9C7AC8E6D523693562F02978B4ADE8F0A995B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_0_0_0_var), NULL);
	}
}
static void U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12__ctor_mF6B169A6528ADBCCEBC674C0B28773255B4DD597(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12_System_IDisposable_Dispose_m3B1E236249AE3937E11BF987AEDF6FAAA47FAF3F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m948B51A90B60A4915C3BCF85B7AF9703559F0115(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12_System_Collections_IEnumerator_Reset_m232752AF09DE77FBA534ECD9C477EA705F0658D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12_System_Collections_IEnumerator_get_Current_mF7E5B2772903E4EF64D175949F66F86842CF6340(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator__speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator__rotationSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__animFrontalShoot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__bulletPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__bulletSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__frontalGun(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__sideRightGuns(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__sideLeftGuns(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__timeBetweenShots(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator_PlayerShoot_StopParticleEffectSideShoot_m59AE2120CEC056AFB7A3D12CA344C74450D71659(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_0_0_0_var), NULL);
	}
}
static void PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator_PlayerShoot_StopParticleEffectFrontShoot_m79BF3B20D0DD0334E19ED056698B3E3C2ED47FD8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_0_0_0_var), NULL);
	}
}
static void U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17__ctor_m0EDF46F07A86C43D7BB7A20CC785D9ED4E815C6D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17_System_IDisposable_Dispose_mB4018D7722BE2F086D92131BF78379CA705A8D8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2416657988A502BC6D7D3857DE7B050BA23733F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17_System_Collections_IEnumerator_Reset_mE44CD0E292E1FEE02F9326387ACF7348BF007555(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17_System_Collections_IEnumerator_get_Current_m70B2EE31947590E53B89FB8649591272729886DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18__ctor_mDD32AAF3222FC81262D87B57F6D4D631E50044C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18_System_IDisposable_Dispose_m167CDDB19475CA486AA0FB82CD6CF1B4ABEAD1F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AA28E30C6546F2B00E5E19286F4CFA1131C94E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_IEnumerator_Reset_m896298A7E7BE8E12821FB96FA2149710F3AC48B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_IEnumerator_get_Current_mF3837D5FF2583A087BEE2EC278E68FE000787929(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__stoppingDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__retreatDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__projectile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__speedRotation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__startTimeBtwShots(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterHealth_t29DF101AD81F8EB494884CD17AEA0DD29E585E5A_CustomAttributesCacheGenerator__enemySprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterHealth_t29DF101AD81F8EB494884CD17AEA0DD29E585E5A_CustomAttributesCacheGenerator__deteriorationEnemy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterHealth_t29DF101AD81F8EB494884CD17AEA0DD29E585E5A_CustomAttributesCacheGenerator__anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShooterHealth_t29DF101AD81F8EB494884CD17AEA0DD29E585E5A_CustomAttributesCacheGenerator_ShooterHealth_WaitStopEffect_m60C7351C6F107A74783EDC0F89A74E8F3387E8DB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_0_0_0_var), NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13__ctor_m4325A1AEF8E24D44DB5D59ED9FC54AA0097468CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_IDisposable_Dispose_m619F2D15380D5AD53059C57749B7AD9E3EAD3767(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m118EAC4268315A97C04F570D43B4E7FD6448DBCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_Reset_mE457C9E0B5212F233CF4801C0C9320B0DC50DB42(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_get_Current_m45FC3FB3CC22D71B8E8C90B684B0B98AE2DD2C9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ShooterProjectile_tD4AF796BCEDC7095B47720654A665E7882432C53_CustomAttributesCacheGenerator__speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[95] = 
{
	U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator,
	U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator,
	U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator,
	U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator,
	U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator,
	U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator,
	U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator,
	CameraClamp_t39FDC82E849AB8EEB2C6D78717894966171C1047_CustomAttributesCacheGenerator__targetToFollow,
	ChaserMovement_tB96C589A2E83EBFDA976369667C93F223DD929E0_CustomAttributesCacheGenerator__speed,
	ChaserMovement_tB96C589A2E83EBFDA976369667C93F223DD929E0_CustomAttributesCacheGenerator__rotationSpeed,
	EndGameManager_t391E87B9BD3657182E84CF31EEC09F8019FF7B27_CustomAttributesCacheGenerator__getWave,
	EndGameManager_t391E87B9BD3657182E84CF31EEC09F8019FF7B27_CustomAttributesCacheGenerator__getTimer,
	EnemyAttack_t770C16819BCC010CF374A277F60DBE4B3F8467E2_CustomAttributesCacheGenerator__cooldownAttack,
	EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator__enemySprite,
	EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator__deteriorationEnemy,
	EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator__anim,
	GameStartText_t6B22BB56DBBF3991534D41C04BF6C17269FA180E_CustomAttributesCacheGenerator__startGameText,
	GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator_EndGameCanvas,
	GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator__Player,
	GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator__spawner,
	GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator__scoreText,
	GameTimer_t34552D41B5A0AFFBBF33ED7216E7D18572BD4A9D_CustomAttributesCacheGenerator__highScore,
	PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_U3CAwareOfPlayerU3Ek__BackingField,
	PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_U3CDirectionToPlayerU3Ek__BackingField,
	PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator__playerAwarenessDistance,
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator__anim,
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator__playerSprite,
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator__deteriorationPlayer,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator__speed,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator__rotationSpeed,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__anim,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__animFrontalShoot,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__bulletPrefab,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__bulletSpeed,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__frontalGun,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__sideRightGuns,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__sideLeftGuns,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator__timeBetweenShots,
	ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__speed,
	ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__stoppingDistance,
	ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__retreatDistance,
	ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__projectile,
	ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__speedRotation,
	ShooterEnemy_tB17C85DB1F4CF5BEA614A7082FC64A8EC8A39198_CustomAttributesCacheGenerator__startTimeBtwShots,
	ShooterHealth_t29DF101AD81F8EB494884CD17AEA0DD29E585E5A_CustomAttributesCacheGenerator__enemySprite,
	ShooterHealth_t29DF101AD81F8EB494884CD17AEA0DD29E585E5A_CustomAttributesCacheGenerator__deteriorationEnemy,
	ShooterHealth_t29DF101AD81F8EB494884CD17AEA0DD29E585E5A_CustomAttributesCacheGenerator__anim,
	ShooterProjectile_tD4AF796BCEDC7095B47720654A665E7882432C53_CustomAttributesCacheGenerator__speed,
	Bullet_tF95A945B732B2B929938FB1028878BFBC0081724_CustomAttributesCacheGenerator_Bullet_WaitBulletDestroy_m0775E3B2D448AE8B6D319E82C6C7344FB8646BDB,
	U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5__ctor_m286BA5208A94E3B54CF808D5ED50F95A7395A434,
	U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5_System_IDisposable_Dispose_mB8D5CAA9F18A1CFBDDC88D46EE09E20F54819BA6,
	U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16261E8717F72F84324127298B394CA7FC29642A,
	U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5_System_Collections_IEnumerator_Reset_mB71FC6F39FE3197B183CB5B2138F320513F1C4B4,
	U3CWaitBulletDestroyU3Ed__5_tA4DB3BE43CF7C501E3462577404883FDA6D54257_CustomAttributesCacheGenerator_U3CWaitBulletDestroyU3Ed__5_System_Collections_IEnumerator_get_Current_mA6AEEB3A2D706D2A817B963E84570C2144CA9EA4,
	EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator_EnemyHealth_WaitStopEffect_m7AFB312BF1F39286E30FBBB8788B19268F6FA150,
	U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13__ctor_m25BAB6225F1E7EC970B81EA7CE7207556BAAAA6F,
	U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_IDisposable_Dispose_mA569F69F32CAAFCE951DEC553845A4A3F361FE30,
	U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C77661935107924964D38F2A329514FE4255C49,
	U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_Reset_mBC6D7E680AA95FA5A01BA2017506EF9F8F765B91,
	U3CWaitStopEffectU3Ed__13_tC039D5091F07142E89DF9A71B12D3E218492B19C_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_get_Current_m050B658A87C16447D307758F9F9612E9D0DD7526,
	GameStartText_t6B22BB56DBBF3991534D41C04BF6C17269FA180E_CustomAttributesCacheGenerator_GameStartText_WaitDestroyText_m0F96D053E959B5A024CECC30351C3F6A997E8E38,
	U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2__ctor_m4EE3707DEBBD9318CD638EC47177E127FE6FBEF8,
	U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2_System_IDisposable_Dispose_m925D7E09D270F2A418F981B44857E9A889B57CC7,
	U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A989809CC89573598A73A570CB9603F7C36FD80,
	U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2_System_Collections_IEnumerator_Reset_m64C3628E3810E960E132B92284D2708E60E236EC,
	U3CWaitDestroyTextU3Ed__2_tF6A280F45CCE4BA9DECC24E1614312E29460E4DD_CustomAttributesCacheGenerator_U3CWaitDestroyTextU3Ed__2_System_Collections_IEnumerator_get_Current_m4E7F0D5E9D8177DC82EEBDF88FD4C6B38BD0BA3B,
	PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_PlayerAwarenessController_get_AwareOfPlayer_m808D36CFC9C67A89EBA9A719F8776B4B42249A37,
	PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_PlayerAwarenessController_set_AwareOfPlayer_mD960C1962FE349F24BB1C0B5B8AB9D462705282D,
	PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_PlayerAwarenessController_get_DirectionToPlayer_m374F8BB97E16ECE6CDA273AE8CDDEF21BC5B1D4B,
	PlayerAwarenessController_t4FF6E0BDB36E080E15BBC8EDFFE364E0806EED47_CustomAttributesCacheGenerator_PlayerAwarenessController_set_DirectionToPlayer_m9A38FAF42D36599FA666ABCC6248F55AA3751D55,
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator_PlayerHealth_DelayPlayerDead_mA6D9C7AC8E6D523693562F02978B4ADE8F0A995B,
	U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12__ctor_mF6B169A6528ADBCCEBC674C0B28773255B4DD597,
	U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12_System_IDisposable_Dispose_m3B1E236249AE3937E11BF987AEDF6FAAA47FAF3F,
	U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m948B51A90B60A4915C3BCF85B7AF9703559F0115,
	U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12_System_Collections_IEnumerator_Reset_m232752AF09DE77FBA534ECD9C477EA705F0658D6,
	U3CDelayPlayerDeadU3Ed__12_t7475DEF702F17459B5E729609ECD95F8392C1ABC_CustomAttributesCacheGenerator_U3CDelayPlayerDeadU3Ed__12_System_Collections_IEnumerator_get_Current_mF7E5B2772903E4EF64D175949F66F86842CF6340,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator_PlayerShoot_StopParticleEffectSideShoot_m59AE2120CEC056AFB7A3D12CA344C74450D71659,
	PlayerShoot_t1054BF57135DEA8FA580C7CD398317B8269F9D14_CustomAttributesCacheGenerator_PlayerShoot_StopParticleEffectFrontShoot_m79BF3B20D0DD0334E19ED056698B3E3C2ED47FD8,
	U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17__ctor_m0EDF46F07A86C43D7BB7A20CC785D9ED4E815C6D,
	U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17_System_IDisposable_Dispose_mB4018D7722BE2F086D92131BF78379CA705A8D8E,
	U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2416657988A502BC6D7D3857DE7B050BA23733F0,
	U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17_System_Collections_IEnumerator_Reset_mE44CD0E292E1FEE02F9326387ACF7348BF007555,
	U3CStopParticleEffectSideShootU3Ed__17_tA26C0ED16F26196B571D8EB288224870F866DEB2_CustomAttributesCacheGenerator_U3CStopParticleEffectSideShootU3Ed__17_System_Collections_IEnumerator_get_Current_m70B2EE31947590E53B89FB8649591272729886DD,
	U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18__ctor_mDD32AAF3222FC81262D87B57F6D4D631E50044C9,
	U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18_System_IDisposable_Dispose_m167CDDB19475CA486AA0FB82CD6CF1B4ABEAD1F6,
	U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AA28E30C6546F2B00E5E19286F4CFA1131C94E4,
	U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_IEnumerator_Reset_m896298A7E7BE8E12821FB96FA2149710F3AC48B2,
	U3CStopParticleEffectFrontShootU3Ed__18_tFCED8F6D8E57C1EACFEF8D3B948A5F6DCEBEC1A8_CustomAttributesCacheGenerator_U3CStopParticleEffectFrontShootU3Ed__18_System_Collections_IEnumerator_get_Current_mF3837D5FF2583A087BEE2EC278E68FE000787929,
	ShooterHealth_t29DF101AD81F8EB494884CD17AEA0DD29E585E5A_CustomAttributesCacheGenerator_ShooterHealth_WaitStopEffect_m60C7351C6F107A74783EDC0F89A74E8F3387E8DB,
	U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13__ctor_m4325A1AEF8E24D44DB5D59ED9FC54AA0097468CA,
	U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_IDisposable_Dispose_m619F2D15380D5AD53059C57749B7AD9E3EAD3767,
	U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m118EAC4268315A97C04F570D43B4E7FD6448DBCC,
	U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_Reset_mE457C9E0B5212F233CF4801C0C9320B0DC50DB42,
	U3CWaitStopEffectU3Ed__13_t8C4907E8E2EA57D29A6548BC2EF0FAF6AF2950CC_CustomAttributesCacheGenerator_U3CWaitStopEffectU3Ed__13_System_Collections_IEnumerator_get_Current_m45FC3FB3CC22D71B8E8C90B684B0B98AE2DD2C9D,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
