using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartText : MonoBehaviour
{
    [SerializeField] private GameObject _startGameText;
    void Start()
    {
        StartCoroutine(WaitDestroyText());
    }

    IEnumerator WaitDestroyText()
    {
        yield return new WaitForSeconds(0.8f);
        Destroy(_startGameText);
    }
}
