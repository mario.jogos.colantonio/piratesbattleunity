using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private EnemyHealth _enemyHealth;
    private ShooterHealth _shooterHealth;
    private GameTimer _score;

    private void Start()
    {
        _enemyHealth = FindObjectOfType<EnemyHealth>();
        _shooterHealth = FindObjectOfType<ShooterHealth>();
        _score = FindObjectOfType<GameTimer>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<EnemyHealth>())
        {
            _enemyHealth = col.GetComponent<EnemyHealth>();
            _enemyHealth.TakeDamage(20);
            Destroy(gameObject);
            if (_enemyHealth._currentHealth <= 0)
            {
                _enemyHealth._colliderDestroy = col;
                _enemyHealth.ExplosionEffect();
                _score.AddScore();
            }
        }
        else
        {
            StartCoroutine(WaitBulletDestroy());
        }

        if (col.GetComponent<ShooterHealth>())
        {
            _shooterHealth = col.GetComponent<ShooterHealth>();
            _shooterHealth.TakeDamage(20);
            Destroy(gameObject);
            if (_shooterHealth._currentHealth <= 0)
            {
                _shooterHealth._colliderDestroy = col;
                _shooterHealth.ExplosionEffect();
                _score.AddScore();
            }
        }
    }
    IEnumerator WaitBulletDestroy()
    {
        yield return new WaitForSeconds(4);
        Destroy(gameObject);
    }
}
