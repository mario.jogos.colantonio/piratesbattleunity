using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField] private Animator[] _anim;
    [SerializeField] private Animator _animFrontalShoot;
    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private float _bulletSpeed;
    [SerializeField] private Transform _frontalGun;
    [SerializeField] private Transform[] _sideRightGuns;
    [SerializeField] private Transform[] _sideLeftGuns;
    [SerializeField] private float _timeBetweenShots;

    private PlayerMovement _canMove;

    private bool _fireContinuously;

    private bool _fireSingle;

    private float _lastFire;

    private void Start()
    {
        _canMove = FindObjectOfType<PlayerMovement>();
        _animFrontalShoot.SetBool("ExplodeCannom", false); 
        for (int i = 0; i < _anim.Length; i++)
        {
            _anim[i].SetBool("ExplodeCannom", false);
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (_fireContinuously || _fireSingle)
            {
                float TimeSinceLastFire = Time.time - _lastFire;

                if (TimeSinceLastFire >= _timeBetweenShots)
                {
                    _animFrontalShoot.SetBool("ExplodeCannom", true); 
                    FrontalBullet();

                    _lastFire = Time.time;
                    _fireSingle = false;
                    StartCoroutine(StopParticleEffectFrontShoot());
                }
            }
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
            if (_fireContinuously || _fireSingle)
            {
                float TimeSinceLastFire = Time.time - _lastFire;

                if (TimeSinceLastFire >= _timeBetweenShots)
                {
                    for (int i = 0; i < _anim.Length; i++)
                    {
                        _anim[i].SetBool("ExplodeCannom", true);
                    }
                    SideBullet();

                    _lastFire = Time.time;
                    _fireSingle = false;
                    StartCoroutine(StopParticleEffectSideShoot());
                }
            }
        }
    }
    private void FrontalBullet()
    {
        if (_canMove._canMove)
        {
            GameObject bullet = Instantiate(_bulletPrefab, _frontalGun.position, transform.rotation);
            Rigidbody2D rigidbody = bullet.GetComponent<Rigidbody2D>();

            rigidbody.velocity = _bulletSpeed * transform.up;
        }
    }
    private void SideBullet()
    {
        if (_canMove._canMove)
        {
            for (int i = 0; i < _sideRightGuns.Length; i++)
            {
                GameObject bullet = Instantiate(_bulletPrefab, _sideRightGuns[i].transform.position,
                    _sideRightGuns[i].transform.rotation);
                Rigidbody2D rigidbody = bullet.GetComponent<Rigidbody2D>();

                rigidbody.velocity = _bulletSpeed * transform.right;
            }

            for (int i = 0; i < _sideLeftGuns.Length; i++)
            {
                GameObject bullet = Instantiate(_bulletPrefab, _sideLeftGuns[i].transform.position,
                    _sideLeftGuns[i].transform.rotation);
                Rigidbody2D rigidbody = bullet.GetComponent<Rigidbody2D>();

                rigidbody.velocity = _bulletSpeed * (transform.right * -1.0f);
            }
        }
    }

    private void OnFire(InputValue inputValue)
    {
        _fireContinuously = inputValue.isPressed;

        if (inputValue.isPressed)
        {
            _fireSingle = true;
        }
    }
    private IEnumerator StopParticleEffectSideShoot()
    {
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < _anim.Length; i++)
        {
            _anim[i].SetBool("ExplodeCannom", false);
        }
    }

    private IEnumerator StopParticleEffectFrontShoot()
    {
        yield return new WaitForSeconds(0.5f);
        _animFrontalShoot.SetBool("ExplodeCannom", false); 
    }
}
