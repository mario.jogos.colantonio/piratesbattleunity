using System.Collections;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private Animator _anim;
    [SerializeField] private SpriteRenderer _playerSprite;
    [SerializeField] private Sprite[] _deteriorationPlayer;
    public int _maxHealth = 100;
    public int _currentHealth;

    public PlayerHealthBar _healthBar;
    private PlayerMovement _movement;
    private GameTimer _gameInfo;
    public bool _playerDie;

    void Start()
    {
        _gameInfo = FindObjectOfType<GameTimer>();
        _movement = FindObjectOfType<PlayerMovement>();
        _currentHealth = _maxHealth;
        _healthBar.SetMaxHealth(_maxHealth);
        _anim.SetBool("CanExplode", false);
    }
    private void Update()
    {
        if (_currentHealth <= 120)
        {
            _playerSprite.sprite = _deteriorationPlayer[0];
        }

        if (_currentHealth <= 60)
        {
            _playerSprite.sprite = _deteriorationPlayer[1];
        }
        
        if(_currentHealth <= 0)
        {
            _movement._canMove = false;
            StartCoroutine(DelayPlayerDead());
        }
    }
    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;
        
        _healthBar.SetHealth(_currentHealth);
    }
    IEnumerator DelayPlayerDead()
    {
        _anim.SetBool("CanExplode", true);
        _playerDie = true;
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
        _gameInfo.EndGame();
    }
}
