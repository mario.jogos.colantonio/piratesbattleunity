using UnityEngine;

public class EndGameManager : MonoBehaviour
{
    [SerializeField] private WaveSpawner _getWave;
    [SerializeField] private GameTimer _getTimer;

    public void ApplyInfoPlayAgain()
    {
        _getWave.timeBetweenWaves = _getWave.timeBetweenWaves;
        _getTimer._limitTime = _getTimer._limitTime;
    }
}
