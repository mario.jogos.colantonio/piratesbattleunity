﻿using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    //public TextMeshProUGUI waveCountText;
    public Transform[] enemyPrefab;
    public float timeBetweenWaves = 5f;
    private float countdown = 2f;
    public int waveNumber = 1;
    public Transform spawnPoint;

    void Update ()
    {
        if(countdown <= 0f)
        {
            SpawnWave();
            countdown = timeBetweenWaves;
        }

        countdown -= Time.deltaTime;
    }
    void SpawnWave()
    {

        for(int i = 0; i < waveNumber; i++)
        {
            SpawnEnemy();
        }
        waveNumber++;
    }

    void SpawnEnemy()
    {
        for (int i = 0; i < enemyPrefab.Length; i++)
        {
            Instantiate(enemyPrefab[i], spawnPoint.position, spawnPoint.rotation);
        }
    }


}
