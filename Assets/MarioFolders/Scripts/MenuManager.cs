using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameObject[] _configButtonReactions;
    public void ConfigButton()
    {
        if (_configButtonReactions[0])
        {
            _configButtonReactions[0].SetActive(false);
        }
        
        if (_configButtonReactions[1])
        {
            _configButtonReactions[1].SetActive(false);
        }
        
        if (_configButtonReactions[2])
        {
            _configButtonReactions[2].SetActive(true);
        }
        
        if (_configButtonReactions[3])
        {
            _configButtonReactions[3].SetActive(true);
        }
        
        if (_configButtonReactions[4])
        {
            _configButtonReactions[4].SetActive(true);
        }
        
        if (_configButtonReactions[5])
        {
            _configButtonReactions[5].SetActive(true);
        }
        
        if (_configButtonReactions[6])
        {
            _configButtonReactions[6].SetActive(true);
        }
    }
    public void BackButton()
    {
        if (_configButtonReactions[0])
        {
            _configButtonReactions[0].SetActive(true);
        }
        
        if (_configButtonReactions[1])
        {
            _configButtonReactions[1].SetActive(true);
        }
        
        if (_configButtonReactions[2])
        {
            _configButtonReactions[2].SetActive(false);
        }
        
        if (_configButtonReactions[3])
        {
            _configButtonReactions[3].SetActive(false);
        }
        
        if (_configButtonReactions[4])
        {
            _configButtonReactions[4].SetActive(false);
        }
        
        if (_configButtonReactions[5])
        {
            _configButtonReactions[5].SetActive(false);
        }
        
        if (_configButtonReactions[6])
        {
            _configButtonReactions[6].SetActive(false);
        }
    }
}
