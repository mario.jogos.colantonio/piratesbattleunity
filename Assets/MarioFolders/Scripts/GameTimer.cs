using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    [SerializeField] private GameObject EndGameCanvas;
    [SerializeField] private GameObject _Player;
    [SerializeField] private GameObject[] _spawner;
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _highScore;
    
    private PlayerMovement _canMove;
    public TextMeshProUGUI _textSeconds;
    public TextMeshProUGUI _textMinuts;
    
    private float _time;
    public int _timeSpeed = 2;
    private int score = 0;
    private int highscore = 0;
    public bool _endGame;
    public float _limitTime;
    private void Start()
    {
        _canMove = FindObjectOfType<PlayerMovement>();
        _scoreText.text = score.ToString() + " SCORE";
        _highScore.text = "HIGHSCORE: " + highscore.ToString();
    }
    void Update()
    {
        CountTimer();
    } 
    private void CountTimer()
    {
        _time += Time.deltaTime * _timeSpeed;
        _textSeconds.text = Mathf.FloorToInt(_time % 60).ToString();
        float mins = _time / 60;

        _textMinuts.text = Mathf.FloorToInt(mins).ToString();
        if (_time >= _limitTime)
        {
            EndGame();
        }
    }
    public void AddScore()
    {
        score += 1;
        _scoreText.text = score.ToString() + " SCORE";
        _highScore.text = "HIGHSCORE: " + score.ToString();
    }
    public void EndGame()
    {
        _canMove._canMove = false;
        gameObject.SetActive(false);
        EndGameCanvas.SetActive(true);
        _Player.SetActive(false);
        _endGame = true;
        for (int i = 0; i < _spawner.Length; i++)
        {
            _spawner[i].SetActive(false);
        }
    }
}
