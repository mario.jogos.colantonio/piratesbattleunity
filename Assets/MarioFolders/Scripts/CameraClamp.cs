using UnityEngine;

public class CameraClamp : MonoBehaviour
{
    [SerializeField] private Transform _targetToFollow;
    private PlayerHealth _checkAlive;

    private void Start()
    {
        _checkAlive = FindObjectOfType<PlayerHealth>();
    }

    void Update()
    {
        if (_checkAlive._playerDie == false)
        {
            transform.position = new Vector3(Mathf.Clamp(_targetToFollow.position.x, -5.5f, 5.5f),
                Mathf.Clamp(_targetToFollow.position.y, -5f, 5f), transform.position.z);
        }
    }
}
