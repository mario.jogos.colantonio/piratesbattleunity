using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationMenu : MonoBehaviour
{
    public void SwitchScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
