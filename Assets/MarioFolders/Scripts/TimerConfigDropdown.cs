using UnityEngine;

public class TimerConfigDropdown : MonoBehaviour
{
    public GameTimer _timer;
    public WaveSpawner _WaveSpawner;

    private void Awake()
    {
        _timer._limitTime = 180.0f;
        _WaveSpawner.timeBetweenWaves = 25f;
    }

    public void HandleInputData(int val)
    {
        if (val == 0)
        {
            _timer._limitTime = 180.0f;
        }
        if (val == 1)
        {
            _timer._limitTime = 360.0f;
        }
        if (val == 2)
        {
            _timer._limitTime = 540.0f;
        }
    }
    
    public void WaveInputData(int val)
    {
        if (val == 0)
        {
            _WaveSpawner.timeBetweenWaves = 25f;
        }
        if (val == 1)
        {
            _WaveSpawner.timeBetweenWaves = 35f;
        }
        if (val == 2)
        {
            _WaveSpawner.timeBetweenWaves = 45f;
        }
    }
}
