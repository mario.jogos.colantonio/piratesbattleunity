using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private float _cooldownAttack;
    private PlayerHealth _playerHealth;
    private float _nextFire;

    private void Start()
    {
        _playerHealth = FindObjectOfType<PlayerHealth>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<PlayerHealth>())
        {
            if (Time.time > _nextFire)
            {
                _nextFire = Time.time + _cooldownAttack;
                _playerHealth.TakeDamage(20);
            }
        }
    }
}
