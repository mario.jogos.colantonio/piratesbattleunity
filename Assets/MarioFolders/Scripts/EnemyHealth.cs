using System.Collections;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _enemySprite;
    [SerializeField] private Sprite[] _deteriorationEnemy;
    [SerializeField] private Animator _anim;
    public int _maxHealth = 60;
    public int _currentHealth;
    public Collider2D _colliderDestroy;

    public PlayerHealthBar _healthBar;
    private GameTimer _timer;
    private PlayerHealth _playerInfo;

    void Start()
    {
        _currentHealth = _maxHealth;
        _healthBar.SetMaxHealth(_maxHealth);
        _timer = FindObjectOfType<GameTimer>();
        _playerInfo = FindObjectOfType<PlayerHealth>();
    }
    private void Update()
    {
        if (_timer._endGame || _playerInfo._playerDie)
        {
            Destroy(gameObject);
        }
        if (_currentHealth <= 40)
        {
            _enemySprite.sprite = _deteriorationEnemy[0];
        }

        if (_currentHealth <= 20)
        {
            _enemySprite.sprite = _deteriorationEnemy[1];
        }
    } 
    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;
        
        _healthBar.SetHealth(_currentHealth);
    }
    public void ExplosionEffect()
    {
        _anim.SetBool("CanExplode", true);
        StartCoroutine(WaitStopEffect());
    }
    IEnumerator WaitStopEffect()
    {
        yield return new WaitForSeconds(0.5f);
        _anim.SetBool("CanExplode", false);
        Destroy(_colliderDestroy.gameObject);
    }
}
