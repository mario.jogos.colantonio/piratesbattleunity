using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;

public class ShooterEnemy : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _stoppingDistance;
    [SerializeField] private float _retreatDistance;
    [SerializeField] private GameObject _projectile;
    [SerializeField] private float _speedRotation;
    [SerializeField] private float _startTimeBtwShots;
    
    private float _timeBtwShots;
    private Vector2 _targetDirection;
    private PlayerAwarenessController _playerAwarenessController;

    private Transform _player;
    private Rigidbody2D _rigidbody;
    void Start()
    {
        _player = FindObjectOfType<PlayerHealth>().transform;

        _rigidbody = GetComponent<Rigidbody2D>();
        _playerAwarenessController = GetComponent<PlayerAwarenessController>();
        _timeBtwShots = _startTimeBtwShots;
    }
    
    void Update()
    {
        LookAtPlayer();
        UpdateTargetDirection();
        ClampCamera();
        if (Vector2.Distance(transform.position, _player.position) > _stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, _player.position, _speed * Time.deltaTime);
        }
        else if (Vector2.Distance(transform.position, _player.position) < _stoppingDistance && Vector2.Distance(transform.position, _player.position) > _retreatDistance)
        {
            transform.position = this.transform.position;
        }
        else if (Vector2.Distance(transform.position, _player.position) < _retreatDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, _player.position, -_speed * Time.deltaTime);
        }

        if (_timeBtwShots <= 0)
        {
            Instantiate(_projectile, transform.position, Quaternion.identity);
            _timeBtwShots = _startTimeBtwShots;
        }
        else
        {
            _timeBtwShots -= Time.deltaTime;
        }
    }

    private void UpdateTargetDirection()
    {
        if (_playerAwarenessController.AwareOfPlayer)
        {
            _targetDirection = _playerAwarenessController.DirectionToPlayer;
        }
        else
        {
            _targetDirection = Vector2.zero;
        }
    }
    private void LookAtPlayer()
    {
        if (_targetDirection == Vector2.zero)
        {
            return;
        }
        Quaternion targetRotation = Quaternion.LookRotation(transform.forward, _targetDirection * -1.0f);
        Quaternion rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, _speedRotation * Time.deltaTime);
        _rigidbody.SetRotation(rotation);
    }

    private void ClampCamera()
    {
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -14f, 14f), Mathf.Clamp(transform.position.y, -10f, 10f));
    }
}
