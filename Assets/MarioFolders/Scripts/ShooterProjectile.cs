using UnityEngine;

public class ShooterProjectile : MonoBehaviour
{
    [SerializeField] private float _speed;
    private Transform _player;
    private Vector2 _target;
    private PlayerHealth _playerHealth;
    void Start()
    {
        _player = FindObjectOfType<PlayerHealth>().transform;

        _target = new Vector2(_player.position.x, _player.position.y);
        _playerHealth = FindObjectOfType<PlayerHealth>();
    }
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, _target, _speed * Time.deltaTime);

        if (transform.position.x == _target.x && transform.position.y == _target.y)
        {
            DestroyProjectile();
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<PlayerHealth>())
        {
            _playerHealth.TakeDamage(20);
            DestroyProjectile();
        }
    }

    private void DestroyProjectile()
    {
        Destroy(gameObject);
    }
}
